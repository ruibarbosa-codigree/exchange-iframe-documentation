﻿# Sports Betting Exchange Whitelabel – iframe integration guide


# Index
[1. Iframe frontend](#_toc153207952)

[1.1. Integration](#_toc153207953)

[1.2. Exchange iframe postMessage](#_toc153207954)

[1.3. Operator domain validation – via iframe postMessage](#_toc153207955)

[1.4. Exchange iframe SRC parameters](#_toc153207956)

[1.5. User login flow](#_toc153207957)

[1.6. User bet placement flow – seamless wallet](#_toc153207958)

[2. Backend server to server communication](#_toc153207959)

[2.1. Server authentication](#_toc153207960)

[2.2. Exchange supplied APIs](#_toc153207961)

[2.2.1. Login](#_toc153207962)

[2.2.2. Logout](#_toc153207963)

[2.2.3. Update user properties](#_toc153207964)

[2.3. Operator supplied APIs](#_toc153207965)

[2.3.1. Transfer](#_toc153207966)

[2.3.2. Offers](#_toc153207967)

[2.3.3. Account statement](#_toc153207968)

[3. Example code](#_toc3examples)

[3.1. Frontend code example - Angular.js](#_toc31exampleangular)

[3.2. Backend code example - Java](#_toc32examplejava)




# <a name="_toc153207952"></a>1. Iframe frontend
## <a name="_toc153207953"></a>1.1. Integration
Operator owns domain “partner.com” where he wants to display the Exchange Iframe.

Operator must create 2 subdomains, on DNS settings, as follows:

|**DNS record type**|**Name**|**Value**|
| :- | :- | :- |
|**CNAME**|exchange1|(***please request us this info***)|
|**CNAME**|exchange2|(***please request us this info***)|

**Important:** please inform us when this is done, so that we can setup the proxy on our side.

Operator should embed in “partner.com” domain and iframe with the following characteristics:

|**Exchange iframe**||
| :- | :- |
|**src**|exchange1.partner.com|
|**width**|100% (as wide as possible, the design is responsive with breakpoint at 1015px)|
|**height**|Recommended minimum height of 500px|

![](imgs/img1-iframe-schema.png)

## <a name="_toc153207954"></a>1.2. Exchange iframe postMessage
Exchange iframe sends postMessage to the parent domain, as listed below.

|**When**|**Message sent**|
| :- | :- |
|**Iframe initial load finish**|data: {type: 'appReady'}|
|**Iframe navigates url**|data: {type: 'routeChange', href: 'sport/1/competition/10932509'}|
|**Iframe changes height needed**|data: {type: 'heightChange', height: 886}|
|**Iframe user session lost**|data: {type: 'sessionExpired'}|

## <a name="_toc153207955"></a>1.3. Operator domain validation – via iframe postMessage
As a security measure, exchange iframe requires the parent domain to send a postMessage after its initial load.

Therefore, parent frame should send wait for receiving message “data: {type: 'appReady'}” and then send the following postMessage to the Exchange iframe:

|**When**|**Message sent**|
| :- | :- |
|**After receiving message data: {type: 'appReady'}**|data: {type: 'checkOrigin'}|

## <a name="_toc153207956"></a>1.4. Exchange iframe SRC parameters
The Exchange iframe supports the following url parameters on the iframe SRC attribute, to set or sync some information from the parent:

|**url parameter**|**example**|**Description**|**Status**|
| :- | :- | :- | :- |
|**lang**|&lang=pt\_br|<p>Language code (ISO 639), underscore, Country code (ISO 3166)</p><p>Note: allowed languages had to be previously agreed, in order to provide full translations.</p>|Optional|
|**href**|&href=sport%2F1|<p>Encoded uri component, for the deep link pretended to show in the iframe. Sport page, event page, market page.</p><p>Should be used the information received from postMessage type:'routeChange'. </p>|Optional|
|**token**||Authenticated user token received from the backend-to-backend “loginV2”.|Required for authenticated users|

Iframe SRC example with all parameters
```
https://exchange1.partner.com/?lang=PT\_BR&href=sport%2F1%2Fcompetition%2F10932509&token=eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIyMjMiLCJleHAiOjE3MDIxOTc3OTUsInVzZXJfcm9sZSI6Ik1lbWJlciIsInVuaXF1ZSI6ImF2T3ZlNXBMIn0.xzR-mKhH2YU8LYHOFoQRS2G-ONsHHP7WPmoGRODRP6arObJ6P4\_g2307vWTdEbX0suBndI1g6MOnu83Vlm5K5g
```

## <a name="_toc153207957"></a>1.5. User login flow
The login in Exchange iframe is done by passing the “token” url parameter in the iframe SRC url.

The token is obtained in the “loginV2” backend-to-backend request.

That token must be forwarded to the frontend, where it will be added in the Exchange iframe SRC url (as the “token” parameter).

Then our Exchange iframe code will validate that token in our backend and authenticates the user, showing adjusted currency and settings. Our code will handle login token renewal, as long as the user is active on the iframe (inactivity for more than 90 minutes may cause user to lose session, and a popup message will show).

## <a name="_toc153207958"></a>1.6. User bet placement flow – seamless wallet
When the authenticated user clicks to place a bet inside Exchange iframe, our frontend communicates with our backend to start the bet placement process.

Because it is a seamless wallet integration: our backend will request your backend for authorization on that risked amount for that user. This is done in a “transfer” request (to the Operator supplied API).

If an error is received from your backend: our iframe will show the corresponding error message to the user.

If a success transfer response is received from your backend: the bet placement process continues on our side.

When bet is accepted, we will inform your backend in a “offers” request (to the Operator supplied API).

Notes:

\- offers information can take from 5 to 10 seconds to reach your server (as it is processed/exported in batches of all orders of all users in that time window);

\- in-play bet placement has 8 to 20 seconds delay, depending on the market characteristics, defined by the international Exchange.


# <a name="_toc153207959"></a>2. Backend server to server communication
## <a name="_toc153207960"></a>2.1. Server authentication
It is required IP Address whitelist of your servers than will send requests to our servers. ***Please inform IPs***.

Please whitelist the following IPs that we will use to send messaged to your “Operator supplied APIs”: (***please request us this info***)

## <a name="_toc153207961"></a>2.2. Exchange supplied APIs
All requests in this section should be done to our api endpoint:

{exchange\_api\_url} = (***please request us this info***)

**Optional:** we send all dates in UTC as default, but Operator can request all dates to be in a different timezone. ***Please inform us the timezone you prefer***.

### <a name="_toc153207962"></a>2.2.1. Login
Method to login a user, either new user or returning user. This will return a token identifying that user in our system, that should be appended in the iframe SRC url within maximum 10 minutes of token creation.

|**POST**|{exchange\_api\_url}/loginV2/|
| :- | :- |

Request headers:

|**Name**|**Required**|**Description**|
| :- | :- | :- |
|**agentUserName**|Required|Provided as an integration credential|
|**agentToken**|Required|Provided as an integration credential|
|**parentUrl**|Required|Operator domain “https://partner.com”|

Request body:

|**Name**|**Required**|**Description**|
| :- | :- | :- |
|**accountId**|Required|Unique identifier of the user, alphanumeric plus "\_", "\|" or "-", case-sensitive, between 6 and 50 chars|
|**ccy**|Required|<p>3 letter currency code (ISO 4217).</p><p>Note: allowed currencies had to be previously agreed.</p>|
|**position**|Required|<p>Position taken: integer number.</p><p>Example, if set to 20 it means operator has 20% on the bets of this user, so when the user places a 10$ bet, only 8$ of his stake is covered by the Exchange.</p>|
|**accountStatus**|Required|<p>AccountStatus enumerator: ACTIVE, INACTIVE.</p><p>Note:<br/>- If is INACTIVE, the user can login and see history bets, but can't place bets.</p>|
|**clientLoginStatus**|Required|<p>ClientLoginStatus enumerator: ON, OFF.</p><p>Note:<br/>- If is OFF, the user can't login.</p>|
|**positionRef**|Optional|<p>String (max 255 chars)</p><p>Example, upline position disribution or any other operator internal reference. Will be returned on Offers Export Api, the positionRef that was set on the user at the time of bet placement.</p>|
|**exposureLimit**|Optional|<p>Integer number</p><p>Exposure limit is max stake open bets.</p><p>Note:<br/>- If not sent:<br/>- - if is 1st login will assume value from parent Agent,<br/>- - if is following logins will not change the value of this user;<br/>- If sent:<br/>- - will be aplied to this user,<br/>- - but if value is higher than parent Agent allows will throw an error.</p>|
|**lossLimit**|Optional|<p>Integer number</p><p>Loss limit is losses in last 24h.</p><p>Note:<br/>- If not sent:<br/>- - if is 1st login will assume value from parent Agent,<br/>- - if is following logins will not change the value of this user;<br/>- If sent:<br/>- - will be aplied to this user,<br/>- - but if value is higher than parent Agent allows will throw an error.</p>|

Response:

|**Case**|**Http status**|**Response body**|
| :- | :- | :- |
|**Success**|200|{baseUrl: "partner.com", token: "eyJhbGciOiJI…UzQoA"}|
|**Error invalid credentials**|401|"Your domain is not valid"|
|**Error wrong agentUserName**|404|"Resource not found"|
|**Error request body format**|400|"Error invalid request data. Field XXXX"|


### <a name="_toc153207963"></a>2.2.2. Logout
We will use the login token passed in the iframe SRC url to authenticate the user and create a cookie. That cookie is valid for 180minutes, and we automatically renew it as long as the iframe SRC url keeps the initial token parameter.

To logout the user, simply reload the our iframe with a SRC url without “token” parameter. We will clear local session information and also call logout to invalidate user session in our server.

There is no server-to-server logout endpoint.


### <a name="_toc153207964"></a>2.2.3. Update user properties
Method to update user properties, while user is loggedin.
Can be used to update the position taken of a user that is already logged in. Useful to update values according to live analysis. Note: will only affect bets placed after the position is changed. 
Can be used to update user accountStatus, usefull to block user from placing more bets.

|**PUT**|{exchange\_api\_url}/updateClientProperties/|
| :- | :- |

Request headers:

|**Name**|**Required**|**Description**|
| :- | :- | :- |
|**agentUserName**|Required|Provided as an integration credential|
|**agentToken**|Required|Provided as an integration credential|
|**parentUrl**|Required|Operator domain “partner.com”|
|**Content-Type**|Required|application/json|

Request body:

|**Name**|**Required**|**Description**|
| :- | :- | :- |
|**accountId**|Required|Unique identifier of the user|
|**ccy**|Required|<p>3 letter currency code (ISO 4217).</p><p>Note: allowed currencies had to be previously agreed.</p>|
|**position**|Required|<p>Position taken: integer number.</p><p>Example, if set to 20 it means operator has 20% on the bets of this user, so when the user places a 10$ bet, only 8$ of his stake is covered by the Exchange.</p>|
|**accountStatus**|Required|<p>AccountStatus enumerator: ACTIVE, INACTIVE.</p><p>Note:<br/>- If is INACTIVE, the user can login and see history bets, but can't place bets.</p>|
|**clientLoginStatus**|Required|<p>ClientLoginStatus enumerator: ON, OFF.</p><p>Note:<br/>- If is OFF, the user can't login.</p>|
|**positionRef**|Optional|<p>String (max 255 chars)</p><p>Example, upline position disribution or any other operator internal reference. Will be returned on Offers Export Api, the positionRef that was set on the user at the time of bet placement.</p>|
|**exposureLimit**|Optional|<p>Integer number</p><p>Exposure limit is max stake open bets.</p><p>Note:<br/>- If not sent:<br/>- - if is 1st login will assume value from parent Agent,<br/>- - if is following logins will not change the value of this user;<br/>- If sent:<br/>- - will be aplied to this user,<br/>- - but if value is higher than parent Agent allows will throw an error.</p>|
|**lossLimit**|Optional|<p>Integer number</p><p>Loss limit is losses in last 24h.</p><p>Note:<br/>- If not sent:<br/>- - if is 1st login will assume value from parent Agent,<br/>- - if is following logins will not change the value of this user;<br/>- If sent:<br/>- - will be aplied to this user,<br/>- - but if value is higher than parent Agent allows will throw an error.</p>|

Response:

|**Case**|**Http status**|**Response body**|
| :- | :- | :- |
|**Success**|200||
|**Error invalid credentials**|401|"Your domain is not valid"|
|**Error wrong agentUserName**|404|"Resource not found"|
|**Error request body format**|400|"Error invalid request data. Field XXXX"|


## <a name="_toc153207965"></a>2.3. Operator supplied APIs
{operator\_api\_url} - example: https://partner.com/api/integration/exchange/

### <a name="_toc153207966"></a>2.3.1. Transfer
We will call this endpoint every time there is a balance update to be made on a user.

Notes:

**\- this endpoint is the ONLY call that should change user balance;**

\- we may send duplicated transfer requests, and Operator should use unique transactionId to deduplicate requests. In case the request is duplicated, we expect the response to be HTTP 200 with body errorCode:"DUPLICATE_TRANSACTION";

\- if we receive response HTTP 4XX or 5XX we will retry to resend the failed transfer, except if our request is bfType:"RESERVATION" (in such case we will show an error to the user who is placing the bet).


Examples:

\- when user places a bet: transfer to debit;

\- when a bet is settled as won/void: transfer to credit.

|**POST**|{operator\_api\_url}/transfer/|
| :- | :- |

Request headers:

|**Name**|**Required**|**Description**|
| :- | :- | :- |
|**Content-Type**|Required|application/json|

Request body:

|**Name**|**Required**|**Description**|
| :- | :- | :- |
|**accountId**|Required|Unique identifier of the user|
|**type**|Required|<p>TransactionType enumerator: CREDIT, DEBIT, DEBIT\_ALLOW.</p><p>Note:</p><p>- If is DEBIT\_ALLOW, the user balance may become negative, as this is needed in resettlement situations.</p>|
|**bfType**|Required|<p>BetfairTransactionType enumerator: RESERVATION, REFUND, SETTLEMENT, RESETTLEMENT.</p><p>- RESERVATION: indicates that this the request for reservation of money for placement<br/>- REFUND: indicates that this the request for release of money during in-play (the bet has been lapsed / voided / etc)<br/>- SETTLEMENT: indicates that this is a request for the alignment of money for settlement<br/>- RESERVATION_IN_PLAY: indicates that this is a request for reserving additional funds due to increased exposure on In-Play market (example when VAR void in-play bets)<br/>- RESETTLEMENT: indicates that this is a request for the alignment of money for resettlement<br/></p>|
|**marketId**|Required|String|
|**lastUpdated**|Required|<p>String (format ISO 8601)<br/>UTC timezone, or other requested by operator</p>|
|**transactionId**|Required|String|
|**amount**|Required|<p>Number</p><p>Up to 2 decimal places.<br/>Can be positive or negative.</p>|
|**description**|Required|<p>String</p><p>On settlement transfers, it is possible to identify bet id it refers to, using the information inside this string.</p><p>On bet placement transfers, the bet id is not yet available because the transfer is requested before placing the bet. The "marketId" info should be used to groups all transfers of 1 user in 1 market.</p>|
|**wasRolledBack**|Required|<p>Boolean</p><p>Can be true only for DEBIT, DEBIT\_ALLOW transactions</p>|
|**frontEndUrl**|Required|<p>String</p><p>It is the parentUrl header sent on the loginV2 call, to help operator with multiple frontend integrations identify the request on his system</p>|

Request example (bet placement):
```
{
  "accountId": "exampleAccountId1",
  "type": "DEBIT",
  "bfType": "RESERVATION",
  "marketId": "1.021564897",
  "lastUpdated": "2023-11-24T12:00:00.000",
  "transactionId": "1",
  "amount": -100.50,
  "description": "Initial Reservation for Placement",
  "wasRolledBack": false,
  "frontEndUrl": "https://partner.com"
}
```

Request example (bet WON):
```
{
  "accountId": "exampleAccountId1",
  "type": "CREDIT",
  "bfType": "SETTLEMENT",
  "marketId": "1.021564897",
  "lastUpdated": "2023-11-24T12:50:08.000",
  "transactionId": "2",
  "amount": 180.90,
  "description": "Settlement of market 1.021564897 / Release reserved funds = 100.50 / 80.40 BRL - SETTLEMENT (19306583)",
  "wasRolledBack": false,
  "frontEndUrl": "https://partner.com"
}
```

Response:

<table><tr><th valign="top"><b>Case</b></th><th valign="top"><b>Name</b></th><th valign="top"><b>Required</b></th><th valign="top"><b>Description</b></th></tr>
<tr><td rowspan="4" valign="top"><p><b>Success</b></p><p><b>HTTP 200</b></p></td><td valign="top">accountId</td><td valign="top">Required</td><td valign="top">Unique identifier of the user</td></tr>
<tr><td valign="top">transactionId</td><td valign="top">Required</td><td valign="top"><p>Unique identifier of the transaction.</p><p>We validate if your response has the same transactionId of our request.</p></td></tr>
<tr><td valign="top">status</td><td valign="top">Required</td><td valign="top">TransactionStatus enumerator: OK, ERROR.</td></tr>
<tr><td valign="top">errorCode</td><td valign="top">Required only if status="ERROR"</td><td valign="top">ErrorResponse enumerator: INSUFFICIENT_FUNDS, ACCOUNT_BLOCKED, BETTING_NOT_ALLOWED, DUPLICATE_TRANSACTION.</td></tr>
<tr><td valign="top"><p><b>Error</b></p><p><b>HTTP 4XX or 5XX</b></p></td><td valign="top"></td><td valign="top"></td><td valign="top"><p>We will retry sending the failed transaction with the following policy:</p><p>- retry every 90 seconds</p><p>Stop after 12 hours of retries.</p><p></p><p>Note: we will not retry sending failed transaction if that was an attempt of user placing a bet (user will have to try to place the bet again manually).</p></td></tr>
</table>

Response example (success):
```
{
  "accountId": "exampleAccountId1",
  "transactionId": "1",
  "status": "OK",
  "errorCode": null
}
```

Response example (duplicated):
```
{
  "accountId": "exampleAccountId1",
  "transactionId": "1",
  "status": "ERROR",
  "errorCode": "DUPLICATE_TRANSACTION"
}
```

### <a name="_toc153207967"></a>2.3.2. Offers
This is only a report export, to give you full details of betting activity. This must not change user balance.

In order to provide redundancy, the feed does not guarantee that individual offer data will not be sent more than once, the receiving system should utilize the unique id being “betId + state + lastUpdated” associated with each record to de-duplicate records upon storage.

Notes:

\- offers information can take from 5 to 10 seconds to reach your server (as it is processed/exported in batches of all orders of all users in that time window);

\- in-play bet placement has 8 to 20 seconds delay, depending on the market characteristics, defined by the international Exchange.

*Offer state diagram*
![](imgs/img2-betfair-bet-state-diagram.png)

|**POST**|{operator\_api\_url}/offers/|
| :- | :- |

Request headers:

|**Name**|**Required**|**Description**|
| :- | :- | :- |
|**Content-Type**|Required|application/json|

Request body: 

Note: it is an ARRAY of the following object

|**Name**|**Required**|**Description**|
| :- | :- | :- |
|**accountId**|Required|Unique identifier of the user|
|**state**|Required|OfferState enumerator: PENDING, PLACED, MATCHED, WON, LOST, CANCELED, LAPSED, VOIDED, EXPIRED.|
|**betId**|Required|Number (format long)|
|**virtualStake**|Required|<p>Number (format big decimal)</p><p>The stake placed by the user (what user see).</p>|
|**realStake**|Required|<p>Number (format big decimal)</p><p>The stake placed on the exchange (risk coverage), which is: realStake = virtualStake * (100 - position)/100</p>|
|**virtualStakeMatched**|Required|Number (format big decimal)|
|**realStakeMatched**|Required|<p>Number (format big decimal)</p><p>Same logic as in *virtualStake* versus *realStake*</p>|
|**odd**|Required|<p>Number (format big decimal, up to 2 decimal places)</p><p>European odds format (from 1.01 to 1000).</p>|
|**averageOddMatched**|Required|<p>Number (format big decimal)</p><p>European odds format.</p><p>Is 0 in case of bet not yet matched.</p>|
|**side**|Required|<p>Boolean</p><p>Notes:</p><p>- if BettingType="LINE": true is “UNDER” false is “OVER”;</p><p>- if other BettingType: true is “BACK” false is “LAY”.</p>|
|**inPlay**|Required|Boolean|
|**marketType**|Required|String|
|**bettingType**|Required|BettingType enumerator: ODDS, LINE, ASIAN\_HANDICAP\_DOUBLE\_LINE, ASIAN\_HANDICAP\_SINGLE\_LINE|
|**lastUpdated**|Required|<p>String (format ISO 8601)<br/>UTC timezone, or other requested by operator</p>|
|**placedDate**|Optional|<p>String (format ISO 8601)<br/>UTC timezone, or other requested by operator<br/>Null in case bet is state:EXPIRED (never had been placed)</p>|
|**virtualProfit**|Optional|<p>Number (format big decimal)</p><p>Null if not yet settled.</p><p>The user profit (what user see), according to the *virtualStakeMatched*.</p>|
|**realProfit**|Optional|<p>Number (format big decimal)</p><p>Null if not yet settled.</p><p>The profit/loss covered in the exchange, which is realProfit = virtualProfit * (100 - position)/100</p>|
|**eventTypeId**|Required|Number (format long)|
|**eventTypeName**|Required|String|
|**competitionId**|Optional|<p>Number (format long)<br/>Null in case of bets in Exchange Games</p>|
|**competitionName**|Optional|<p>String<br/>Null in case of bets in Exchange Games</p>|
|**eventId**|Required|String|
|**eventName**|Required|String|
|**marketStartTime**|Required|String (format ISO 8601)<br/>UTC timezone, or other requested by operator|
|**marketId**|Required|String|
|**marketName**|Required|String|
|**selectionId**|Required|Number (format long)|
|**selectionName**|Required|String|
|**handicap**|Required|<p>Number (format big decimal)</p><p>Is "0.00" when not applicable to the market. But may be "0.00" when is applicable to that market.</p>|
|**currency**|Required|3 letter currency code (ISO 4217)|
|**frontEndUrl**|Required|<p>String</p><p>It is the parentUrl header sent on the loginV2 call, to help operator with multiple frontend integrations identify the request on his system</p>|
|**settledDate**|Optional|String (format ISO 8601)<br/>UTC timezone, or other requested by operator|
|**positionRef**|Optional|<p>String (max 255 chars)</p><p>Example, upline position disribution or any other operator internal reference sent on user login (or user update position call) at the time of bet placement.</p>|

Request example:
```
{
  "accountId": "exampleAccountId1",
  "state": "PLACED",
  "betId": "2",
  "virtualStake": "10.00",
  "realStake": "8.00",
  "virtualStakeMatched": "0",
  "realStakeMatched": "0",
  "odd": "2.10",
  "averageOddMatched": "0",
  "side": "true",
  "inPlay": "false",
  "marketType": "MATCH\_ODDS",
  "bettingType": "ODDS",
  "lastUpdated": "2023-11-24T12:00:03.000",
  "placedDate": "2023-11-24T12:00:00.000",
  "virtualProfit": "null",
  "realProfit": "null",
  "eventTypeId": "1",
  "eventTypeName": "Soccer",
  "competitionId": "1081960",
  "competitionName": "French National",
  "eventId": "32828953",
  "eventName": "GOALyonnaise v Dijon",
  "marketStartTime": "2023-11-24T12:30:00.000",
  "marketId": "1.222016091",
  "marketName": "Match Odds",
  "selectionId": "58805",
  "selectionName": "The Draw",
  "handicap": "0.00",
  "currency": "USD",
  "frontEndUrl": "https://partner.com",
  "settledDate": null,
  "positionRef": "10|50|20"
}
```

Response:

|**Case**|**Response body**|**Description**|
| :- | :- | :- |
|<p>**Success**</p><p>**HTTP 200**</p>|*empty*||
|<p>**Error**</p><p>**HTTP 4XX or 5XX**</p>|*empty*|<p>We will retry sending the failed offers list with the following policy:</p><p>- retry every 90 seconds</p><p>Stop after 12 hours of retries.</p>|


### <a name="_toc153207968"></a>2.3.3. Account statement
This is only a report export, to give you full details of user balance changes done by the transfer endpoint. This must not change user balance.

In order to provide redundancy, the feed does not guarantee that individual account statement records will not be sent more than once, the receiving system should utilize the unique id being “statementId + entryDate” associated with each record to de-duplicate records upon storage.

Note:

\- account statement records information can take from 5 to 10 seconds to reach your server (as it is processed/exported in batches of all statements of all users in that time window).

|**POST**|{operator\_api\_url}/statements/|
| :- | :- |

Request headers:

|**Name**|**Required**|**Description**|
| :- | :- | :- |
|**Content-Type**|Required|application/json|

Request body: 

Note: it is an ARRAY of the following object

|**Name**|**Required**|**Description**|
| :- | :- | :- |
|**accountId**|Required|Unique identifier of the user|
|**statementId**|Required|Number (format long)|
|**entryDate**|Required|<p>String (format ISO 8601)<br/>UTC timezone, or other requested by operator</p>|
|**origin**|Required|AccountStatement enumerator: DEPOSIT, WITHDRAWAL, SETTLEMENT, CORRECTION, RESETTLEMENT, CHARGE\_COMMISSION, CHARGE\_TURNOVER, CHARGE\_TRANSACTION|
|**description**|Required|String|
|**originId**|Required|<p>String</p><p>Notes:</p><p>- if origin="CHARGE\_COMMISSION": originId is the marketId (and if is an “handicap” offer, the field originId is marketId+\"\|\"+handicap)</p><p>- if other origin: originId is the betId from the offers endpoint.</p>|
|**debit**|Required|Number (format big decimal)|
|**credit**|Required|Number (format big decimal)|
|**currency**|Required|3 letter currency code (ISO 4217)|
|**commissionRate**|Required only if origin= "CHARGE\_COMMISSION"|Number (format double, up to 2 decimal places)|
|**frontEndUrl**|Required|<p>String</p><p>It is the parentUrl header sent on the loginV2 call, to help operator with multiple frontend integrations identify the request on his system</p>|

Request example lost bet:
```
{
  "accountId": "exampleAccountId1",
  "statementId": "1",
  "entryDate": "2023-11-24T12:00:00.000",
  "origin": "SETTLEMENT",
  "description": "English Championship / Huddersfield v QPR / Over/Under 2.5 Goals / Over 2.5 Goals",
  "originId": "2",
  "debit": "10.00",
  "credit": "0.00",
  "currency": "USD",
  "commissionRate": null,
  "frontEndUrl": "https://partner.com"
}
```

Request example commission charged on market profit:
```
{
  "accountId": "exampleAccountId1",
  "statementId": "2",
  "entryDate": "2023-11-24T12:00:00.000",
  "origin": "CHARGE\_COMMISSION",
  "description": "English Championship / Huddersfield v QPR / Over/Under 2.5 Goals - Commission",
  "originId": "1.219384635",
  "debit": "6.24",
  "credit": "0.00",
  "currency": "USD",
  "commissionRate": "6.0",
  "frontEndUrl": "https://partner.com"
}
```

Response:

|**Case**|**Response body**|**Description**|
| :- | :- | :- |
|<p>**Success**</p><p>**HTTP 200**</p>|*empty*||
|<p>**Error**</p><p>**HTTP 4XX or 5XX**</p>|*empty*|<p>We will retry sending the failed account statement records list with the following policy:</p><p>- retry every 90 seconds</p><p>Stop after 12 hours of retries.</p>|


# <a name="_toc3examples"></a>3. Example code
## <a name="_toc31exampleangular"></a>3.1. Frontend code example - Angular.js
We have built a simple frontend app in Angular.js, that implements the comunication between a sample backend (see [3.2. Backend code example - Java](#_toc32examplejava)) to login the user and correctly mount our Exchange iframe.

It also implements "on message" and "post message" comunicaation with our Exchange iframe.

Sample code in:
```
https://gitlab.com/ruibarbosa-codigree/exchange-iframe-frontend-example
```

## <a name="_toc32examplejava"></a>3.2. Backend code example - Java
We have built a simple backend project in Java (with Spring Boot framework), that implements the comunication with our backend. Implementing calls made to our [Exchange supplied APIs](#_toc153207961) and also implementing the [Operator supplied APIs](#_toc153207965) that we need to call.

Sample code in:
```
https://gitlab.com/ruibarbosa-codigree/exchange-iframe-backend-example
```


<p> </p>
<p> </p>
___
___

Exchange iframe integration guide		v20231222